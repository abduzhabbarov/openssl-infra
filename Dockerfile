FROM alpine:latest

# Copy OpenSSL artifacts from your build directory to the image
COPY ./.. /app
